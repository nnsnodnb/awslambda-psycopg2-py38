# awslambda-psycopg2-py38

This is a custom compiled psycopg2 C library for Python. Due to AWS Lambda missing the required PostgreSQL libraries in the AMI image, we needed to compile psycopg2 with to PostgreSQL `libpq.so` library statcally linked instead of the default dynamic link.

## Environment

- AMI ID `ami-052652af12b58691f`
- Python 3.8

## How to use

- Just copy `psycopg2` directory into your AWS Lambda zip package.
- Archive `psycopg2` directory and upload to AWS Lambda Layer.
  - You must add `PYTHONPATH=/opt` environment variable in your AWS Lambda function.

## Instructions on compiling this package from scratch

1. Download the [PostgreSQL source code](https://ftp.postgresql.org/pub/source/v11.4/postgresql-11.4.tar.gz) and extract into a directory.
1. Download the [psycopg2 source code](http://initd.org/psycopg/tarballs/PSYCOPG-2-8/psycopg2-2.8.4.tar.gz) and extract into a directory.
1. Go into the PostgreSQL source directory and execute the following commands:

    ```
    $ PG_DIR=/tmp/pg
    $ ./configure --prefix $PG_DIR --without-readlib --without-zlib
    $ make
    $ make install
    ```

4. Go into the psycopg2 source directory and execute the following commands:

    ```bash
    $ sed -i -e s'/pg_config =/pg_config = \/tmp\/pg\/bin\/pg_config/' setup.cfg
    $ sed -i -e s'/static_libpq = 0/static_libpq = 1/' setup.cfg
    $ LD_LIBRARY_PATH=$PG_DIR/lib:$LD_LIBRARY_PATH python setup.py build
    ```
